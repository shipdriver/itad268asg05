﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace RRinas_ITAD268_Asg05_03
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_03_01.cs";
            p.Start();

            p.WaitForExit();
            Process pa = new Process();
            pa.StartInfo.FileName = @"c:\Test\asg05.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_03_02.cs";
            p.Start();

            p.WaitForExit();
            Process pa = new Process();
            pa.StartInfo.FileName = @"c:\Test\asg05.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod3()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_03_03.cs";
            p.Start();

            p.WaitForExit();
            Process pa = new Process();
            pa.StartInfo.FileName = @"c:\Test\asg05.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod4()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_03_04.cs";
            p.Start();

            p.WaitForExit();
            Process pa = new Process();
            pa.StartInfo.FileName = @"c:\Test\asg05.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod5()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_03_05.cs";
            p.Start();

            p.WaitForExit();
            Process pa = new Process();
            pa.StartInfo.FileName = @"c:\Test\asg05.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod6()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:c:\Test\asg05.exe c:\Test\RRinas_ITAD268_Asg05_04_06.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();


            p.WaitForExit();
            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));
        }

       
    }
}
